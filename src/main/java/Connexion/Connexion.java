package Connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Agnein
 *
 */
public class Connexion {
//ATTRIBUT
	private String adress = "jdbc:postgresql://localhost:5432/Hotel"; 
	private String name= "Hotel_manager"; 
	private String password ="afpa123"; 
	private String driver = "org.postgresql.Driver"; 
	private Connection connex; 
	
	
	//CONSTRUCTEUR
	public Connexion(String adress,String name, String password, String driver) {
		super(); 
		this.adress=adress; 
		this.name=name;
		this.password=password;
		this.driver=driver;
		try {
			//il faut charger le driver pour établir la connexion a chaque fois
			Class.forName("org.postgresql.Driver");
			//on donne a la connection les parametres qui vont lui permettre de s'etablir
			this.connex= DriverManager.getConnection(adress, name, password); 
			//on permet d'ecrire directement dans la BDD sans avoir a confirmer le commit
			connex.setAutoCommit(true);
			
		}catch (SQLException e){// Si on a une erreur SQL
			e.printStackTrace();
			
		}catch (ClassNotFoundException e) {//si la classe demande n'est pas bien definie
			e.printStackTrace();
			
		}
	}
	//METHODE
	
	//getter de connex
	public Connection getCo(){
		return connex;
	}
	
	//methode de fermeture de connexion
	public void closeConnection() {
		try {
			this.connex.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	

}
