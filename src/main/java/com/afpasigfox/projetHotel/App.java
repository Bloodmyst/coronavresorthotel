package com.afpasigfox.projetHotel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Connexion.Connexion;
import model.beans.Categorie;
import model.beans.Chambre;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.dao.DAO;
import model.dao.DaoCategorie;
import model.model.Collectionnable;
import model.model.DAOFactory;
import model.dao.DaoChambre;
import model.dao.DaoHotel;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{
		//TODO faire classe abstraite de la DAO pour rassembler les methodes
		
		//J'instancie la connexion a ma BDD
		Connexion connect = new Connexion("jdbc:postgresql://localhost:5432/Hotel", "Hotel_manager", "afpa123", "org.postgresql.Driver");

		//J'instancie mon Usine DAO
		DAOFactory monUsineADao = new DAOFactory(connect); 


		// Methode d'insert
		/*monUsineADao.getDAO(monUsineADao.DaoCategorie).create(new Categorie(11, "Extreme Luxe"));
		monUsineADao.getDAO(monUsineADao.DaoTypeHotel).create(new TypeHotel(10,"10 étoiles"));
		monUsineADao.getDAO(monUsineADao.DaoHotel).create(new Hotel(7,new TypeHotel(9,"10 étoiles")
		,"SuperStar hotel", "la-bas", "19900", "trifouillis"));
		((DaoChambre)monUsineADao.getDAO(monUsineADao.DaoChambre)).create(new Chambre(0, 1, new Categorie(11, "Extreme Luxe")), 1); */

		//Methode d'Update
		/*monUsineADao.getDAO(monUsineADao.DaoCategorie).update( new Categorie(10, "Just Luxe"));
		monUsineADao.getDAO(monUsineADao.DaoTypeHotel).update( new TypeHotel(7, "8 étoiles"));
		monUsineADao.getDAO(monUsineADao.DaoHotel).update(new Hotel (new TypeHotel(9,"10 étoiles"),"Vr resort", "15 rue des baleines", "20000", "Trifouillis les petzouilles",6));
		((DaoChambre)monUsineADao.getDAO(monUsineADao.DaoChambre)).update(new Chambre(0, 1, new Categorie(11, "Extreme Luxe")), 1);*/
		
		//Methode de FindById
		//System.out.println(monUsineADao.getDAO(DAOFactory.DaoCategorie).findById(3));	
		/*System.out.println(monUsineADao.getDAO(monUsineADao.DaoTypeHotel).findById(0));	
		System.out.println(monUsineADao.getDAO(monUsineADao.DaoHotel).findById(6));	
		System.out.println(monUsineADao.getDAO(monUsineADao.DaoChambre).findById(0));*/

		//Methode de FindAll
		//System.out.println(monUsineADao.getDAO(monUsineADao.DaoCategorie).findAll());
		//System.out.println(monUsineADao.getDAO(monUsineADao.DaoTypeHotel).findAll());
		//System.out.println(monUsineADao.getDAO(monUsineADao.DaoHotel).findAll());
		//System.out.println(monUsineADao.getDAO(monUsineADao.DaoChambre).findAll());

		//Methode de Delete one item
		/*monUsineADao.getDAO(monUsineADao.DaoCategorie).delete(7);
		monUsineADao.getDAO(monUsineADao.DaoTypeHotel).delete(7);
		monUsineADao.getDAO(monUsineADao.DaoHotel).delete(7);
		monUsineADao.getDAO(monUsineADao.DaoChambre).delete(7);*/
		
		
		


	}
}
