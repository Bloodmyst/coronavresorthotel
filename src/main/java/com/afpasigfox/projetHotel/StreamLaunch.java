package com.afpasigfox.projetHotel;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import Connexion.Connexion;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.dao.DAO;
import model.dao.DaoHotel;
import model.dao.DaoTypeHotel;
import model.model.Collectionnable;
import model.model.Collectionneur;
import model.model.DAOFactory;

public class StreamLaunch {


	public static void main( String[] args )
	{
		//J'instancie la connexion a ma BDD
		Connexion connect = new Connexion("jdbc:postgresql://localhost:5432/Hotel", "Hotel_manager", "afpa123", "org.postgresql.Driver");
		
		
		//J'instancie mon Usine DAO
		DAOFactory monUsineADao = new DAOFactory(connect); 



		//Approche declarative des streams	
		//Filter 
		// a partir tous mes hotels je veux recuperer une
		//		System.out.println(monUsineADao.getDAO(monUsineADao.DaoHotel).findAll().getItem(2).getId());; 
		//		monUsineADao.getDAO(monUsineADao.DaoHotel).findAll().getCollectionToHM() 
		//		.entrySet().stream()
		//		.filter(elt -> elt.getKey().equal( 2))				
		//		.forEach(v-> System.out.println(v.getValue()));
		//		//.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


		//Version simplifier TODO modifier le getName()
		//respecter la casse dans la contains ou forcer en minuscule
		new DaoHotel(connect)
		.findAll().getCollectionToHM() 
		.entrySet().stream()
		.filter(elt ->elt.getValue().getNomHotel().contains("FONTAINE"))
		.map (map -> map.getValue())
		.forEach(v -> System.out.println(v.getNomHotel())); 



		//Version avec un if 
		//		MapHotel.entrySet().stream()
		//		.filter(elt -> {
		//			if (elt.getValue().getName().contains("FONTAINE")) {
		//				return true; 
		//			}else {
		//				return false; 
		//			}
		//		}
		//				)
		//		.map (map -> map.getValue())
		//		.forEach(v -> System.out.println(v.getName())); 
		//	}



		//Sort 
		//	All match
		//	Any match
		//	non match 
		//	Max
		//	Min 
		//	Group
	}
}