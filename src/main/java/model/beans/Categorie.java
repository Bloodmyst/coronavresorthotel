package model.beans;

import model.model.Collectionnable;

public class Categorie extends Collectionnable {
	//ATTRIBUT
	private String nomCat; 


	//CONSTRUCTEUR
	
	public Categorie () {
		
	}
	/**
	 * @param string
	 */
	public Categorie(String string) {
	}
	/**
	 * @param nuCat
	 * @param nomCat
	 */
	public Categorie (Integer nuCat, String nomCat) {
		super(nuCat); 
		this.nomCat=nomCat; 
	}
	//GETTER SETTER

	
	public String getNomCat() {
		return nomCat;
	}
	public void setNomCat(String nomCat) {
		this.nomCat = nomCat;
	}
	
	
	//METHODE
	@Override
	public String toString() {
		return "Categorie [nomCat=" + nomCat + "]";
	}
	
}