package model.beans;

import model.model.Collectionnable;

public class Chambre extends Collectionnable {
	//ATTRIBUT
		private  Categorie CatChambre; 
		private Integer nuChambre; 
		
		//CONSTRUCTEUR
		public Chambre() {
			super();
		}
		
		/**
		 * @param nuChambre
		 * @param CatChambre
		 */
		public Chambre(int id, Integer nuChambre , Categorie CatChambre) {
			super(id); 
			this.nuChambre=nuChambre; 
			this.CatChambre=CatChambre; 
		}

		

		public Chambre(Integer nuChambre) {
			super(nuChambre); 
		}
		//GETTER SETTER
		/**
		 * @return
		 */
		public Integer getNuChambre() {
			return nuChambre;
		}
		public void setNuChambre(Integer nuChambre) {
			this.nuChambre = nuChambre;
		}
		
	
		public Categorie getCatChambre() {
			return CatChambre;
		}
		public void setCatChambre(Categorie catChambre) {
			CatChambre = catChambre;
		}
		
		//METHODE

		@Override
		public String toString() {
			return "Chambre [CatChambre=" + CatChambre + ", nuChambre=" + nuChambre + "]";
		}

}
