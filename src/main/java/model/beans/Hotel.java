package model.beans;

import model.model.Collectionnable;

public class Hotel extends Collectionnable   {
	
	//ATTRIBUT
	private TypeHotel nuTypeHotel;
	private String nomHotel; 
	private String adressHotel; 
	private String cpHotel; 
	private String villeHotel;


	//CONSTRUCTEUR
	
	/**
	 * @param nuHotel
	 * @param nuTypeHotel
	 * @param nomHotel
	 * @param adressHotel
	 * @param cpHotel
	 * @param villeHotel
	 */
	public Hotel (Integer nuHotel,TypeHotel nuTypeHotel, String nomHotel, String adressHotel, String cpHotel, String villeHotel){
		super(nuHotel); 
		this.nuTypeHotel= nuTypeHotel; 
		this.nomHotel= nomHotel; 
		this.adressHotel=adressHotel;
		this.cpHotel= cpHotel; 
		this.villeHotel=villeHotel; 

	}

	//GETTER SETTER
	
	public Hotel(TypeHotel nuTypeHotel, String nomHotel, String adressHotel, String cpHotel, String villeHotel, Integer nuHotel) {
		super(nuHotel); 
		this.nuTypeHotel= nuTypeHotel; 
		this.nomHotel= nomHotel; 
		this.adressHotel=adressHotel;
		this.cpHotel= cpHotel; 
		this.villeHotel=villeHotel; 
	}

	public TypeHotel getTypeHotel() {
		return nuTypeHotel;
	}

	public void setTypeHotel(TypeHotel nuTypeHotel) {
		this.nuTypeHotel = nuTypeHotel;
	}

	
	public String getNomHotel() {
		return nomHotel;
	}
	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}
	public String getAdressHotel() {
		return adressHotel;
	}
	public void setAdressHotel(String adressHotel) {
		this.adressHotel = adressHotel;
	}
	public String getCpHotel() {
		return cpHotel;
	}
	public void setCpHotel(String cpHotel) {
		this.cpHotel = cpHotel;
	}
	public String getVilleHotel() {
		return villeHotel;
	}
	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}

	@Override
	public String toString() {
		return "Hotel [nuTypeHotel=" + nuTypeHotel + ", nomHotel=" + nomHotel + ", adressHotel=" + adressHotel
				+ ", cpHotel=" + cpHotel + ", villeHotel=" + villeHotel + "]";
	}


	//METHODE
}
