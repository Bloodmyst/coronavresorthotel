package model.beans;

import model.model.Collectionnable;

public class TypeHotel extends Collectionnable {
	
		//ATTRIBUT
		private String nomType;
		
		
		
		//CONSTRUCTEUR
		/**
		 * @param nuType
		 * @param nomType
		 */
		public TypeHotel(Integer nuType, String nomType) {
			super(nuType); 
			this.nomType= nomType; 
		}
	
		//GETTER SETTER
		/**
		 * @return
		 */
		public String getNomType() {
			return nomType;
		}
		public void setNomType(String nomType) {
			this.nomType = nomType;
		} 
		
		//METHODE
		@Override
		public String toString() {
			return "TypeHotel [nomType=" + nomType + "]";
		}
}
