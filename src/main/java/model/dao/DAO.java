package model.dao;

import Connexion.Connexion;
import model.beans.TypeHotel;
import model.model.Collectionnable;
import model.model.Collectionneur;

public abstract class DAO<T extends Collectionnable>  {
	//ATTRIBUTS
	private Connexion connex = null; 
	//CONSTRUCTEUR
	/**
	 * @param pConnex
	 */
	public DAO(Connexion pConnex) {
		this.connex= pConnex; 
	}


	//GETTER SETTER

	protected Connexion getConnex() {
		return this.connex; 
	}


	//METHODES
	//penser a rajouter les exception quand sera fait la classe exception throws MyException
	//ma dao prevoir des methodes en rapport avec mes requetes a ma BDD
	public abstract T			create(T 	objACreer) ;
	public abstract T 			update(T 	objAModifier);
	public abstract void 		delete(int 	idADeleter);
	public abstract void 		delete(T 	objADeleter);
	public abstract T			findById(int idATrouver); 
	public abstract Collectionneur<T> findAll();
}
