package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Connexion.Connexion;
import model.beans.Categorie;
import model.beans.TypeHotel;
import model.model.Collectionneur;

public class DaoCategorie extends DAO<Categorie> {

	//ATTRIBUTS
	private static final String REQCREATE = "INSERT INTO CATEGORIE (nucat, nomcat) VALUES(?,?)";
	private static final String REQUPDATE = "UPDATE CATEGORIE SET nomcat = ? WHERE nucat =  ?";
	private static final String REQDELETE = "DELETE FROM CATEGORIE WHERE nucat = ?";
	private static final String REQFINDBYID = "SELECT * FROM CATEGORIE WHERE nucat = ?";
	private static final String REQFINDALL = "SELECT * FROM CATEGORIE"; 
	//private static final String REQDELETE2 = "DELETE * FROM CATEGORIE";


	//CONSTRUCTEUR
	public DaoCategorie(Connexion pConnex) {
		super(pConnex);
	}

	@Override
	public Categorie create(Categorie objACreer) {

		Categorie categorie = null; 
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQCREATE);
			stmt.setInt(1, objACreer.getId());
			stmt.setString(2, objACreer.getNomCat());
			//On execute la creation et si il y a un insert on retourne un message dans la console
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("une catégorie a ete cree");
			}
		} catch (SQLException e) {
		}

		return categorie;
	}

	@Override
	public Categorie update(Categorie objAModifier) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQUPDATE);
			stmt.setString(1, objAModifier.getNomCat());
			stmt.setInt(2, objAModifier.getId());
			//On execute l'update et si il y a une update on retourne un message dans la console
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("une categorie a ete mise a jour");
			}
		} catch (SQLException e) {
		}
		return null;
	}
	@Override
	public void delete(int idADeleter) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQDELETE);
			stmt.setInt(1, idADeleter);
			stmt.executeUpdate();

		}catch (SQLException e) {
		}

	}

	@Override
	public void delete(Categorie objADeleter) {
		// TODO Auto-generated method stub

	}

	@Override
	public Categorie findById(int idATrouver) {
		Categorie categorie = null; 
		try  {
			PreparedStatement stmt 	= getConnex().getCo().prepareStatement
					(REQFINDBYID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setInt(1, idATrouver);
			ResultSet rs	= stmt.executeQuery();
			
			if (rs.first()) {
				categorie = new Categorie(rs.getInt("nucat"), 
						rs.getString("nomcat")
						);
				System.out.println(categorie);
			}
		}catch (SQLException e) {
			e.printStackTrace();}
		return categorie;
	}

	@Override
	public Collectionneur<Categorie> findAll() {
		Collectionneur<Categorie> categorie = new Collectionneur<Categorie>(); 
		try {PreparedStatement stmt 	= getConnex().getCo().prepareStatement
				(REQFINDALL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet rs	= stmt.executeQuery();
		while (rs.next()) {
			Categorie cats = new Categorie(rs.getInt("nucat"), 
					rs.getString("nomcat")
					);
			categorie.addItem(cats);
		}
		}catch (SQLException e){
			e.printStackTrace();}
		return categorie;
	}




}
