package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Connexion.Connexion;
import model.beans.Categorie;
import model.beans.Chambre;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.model.Collectionneur;

public class DaoChambre extends DAO<Chambre>{
	//ATTRIBUTS
	private static final String REQCREATE = "INSERT INTO CHAMBRE (idchambre, nuchambre, nucatchambre,nuhotel) VALUES(?,?,?,?)";
	private static final String REQUPDATE = "UPDATE CHAMBRE SET nuchambre = ?, nucatchambre = ?, nuhotel = ?  WHERE (idchambre = ? AND nuhotel = ?)";
	private static final String REQDELETE = "DELETE FROM CHAMBRE (WHERE nuchambre = ? AND nuhotel = ?)";
	private static final String REQFINDBYID = "SELECT * FROM CHAMBRE WHERE nuchambre = ? AND nuhotel = ?";
	private static final String REQFINDALL = "SELECT * FROM CHAMBRE"; 
	private static final String REQDELETE2 = "DELETE * FROM CHAMBRE";

	//CONSTRUCTEUR
	public DaoChambre(Connexion pConnex) {
		super(pConnex);
	}

	public Chambre create(Chambre objACreer, int numHotel) {
		try {
			System.out.println("coucou");
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQCREATE);
			stmt.setInt(1, objACreer.getId());
			stmt.setInt(2, objACreer.getNuChambre());
			stmt.setInt(3, objACreer.getCatChambre().getId());
			stmt.setInt(4,numHotel);
			System.out.println(objACreer.getId()+" "+objACreer.getCatChambre().getId()+" "+objACreer.getCatChambre().getId());
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("une chambre a ete cree");
			}
		} catch (SQLException e) {
		}

		return null;
	}


	public Chambre update(Chambre objAModifier, int numHotel) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQUPDATE);
			
			stmt.setInt(1, objAModifier.getNuChambre());
			stmt.setInt(2, objAModifier.getCatChambre().getId());
			stmt.setInt(3,numHotel);
			stmt.setInt(4, objAModifier.getId());
			stmt.setInt(4, objAModifier.getId());
			
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("une chambre a ete mise a jour");
			}
		} catch (SQLException e) {
		}

		return null;
	}

	@Override
	public void delete(int idADeleter) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQDELETE);
			stmt.setInt(1, idADeleter);
			stmt.executeUpdate();

		}catch (SQLException e) {
		}
	}

	@Override
	public void delete(Chambre objADeleter) {
		// TODO Auto-generated method stub

	}

	@Override
	public Chambre findById(int idATrouver) {
		Chambre chambre = null; 
		Categorie categorie = null;
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQFINDBYID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setInt(1, idATrouver);
			stmt.setInt(2, idATrouver);			
			ResultSet rs = stmt.executeQuery();
			System.out.println(rs.getInt("nucatchambre"));
			if (rs.first()) {
				categorie = new DaoCategorie(getConnex()).findById(rs.getInt("nucatchambre"));
				chambre = new Chambre();				
			}
		}catch (SQLException e) {;
		e.printStackTrace();
		}
		return chambre;
	}

	@Override
	public Collectionneur<Chambre> findAll() {
		Collectionneur<Chambre> TChambre = new Collectionneur<Chambre>(); 
		try  {
			PreparedStatement stmt 	= getConnex().getCo().prepareStatement
					(REQFINDALL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs	= stmt.executeQuery();
			while (rs.next()) {
				TChambre.addItem(new Chambre(rs.getInt("idchambre"),rs.getInt("nuChambre"), new DaoCategorie(getConnex()).findById(rs.getInt("nuCatChambre")))); 
			}
		}catch (SQLException e) {
			e.printStackTrace();}
		return TChambre; 

	}

@Override
	public Chambre create(Chambre objACreer) {
	System.out.println("null");
		return null;
	}

@Override
public Chambre update(Chambre objAModifier) {
	// TODO Auto-generated method stub
	return null;
}













}
