package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Connexion.Connexion;
import model.beans.Chambre;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.model.Collectionneur;

public class DaoHotel extends DAO<Hotel> {
	//ATTRIBUTS
	private static final String REQCREATE = "INSERT INTO HOTEL (nuhotel,nutypehotel, nomhotel,adressehotel, cphotel, villehotel ) VALUES(?,?,?,?,?,?)";
	private static final String REQUPDATE = "UPDATE HOTEL SET nutypehotel = ?, nomhotel = ?, adressehotel = ?, cphotel = ?,villehotel = ? WHERE nuhotel = ?"; 
	private static final String REQDELETE = "DELETE FROM HOTEL WHERE nuhotel = ?";
	private static final String REQFINDBYID = "SELECT * FROM HOTEL WHERE nuhotel = ?";
	private static final String REQFINDALL = "SELECT * FROM HOTEL"; 


	//CONSTRUCTEUR
	public DaoHotel(Connexion pconnex) {
		super(pconnex); }


	@Override
	public Hotel create(Hotel objACreer) {
		Hotel hotel = null; 
		try {

			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQCREATE);
			stmt.setInt(1, objACreer.getId());
			stmt.setInt(2, objACreer.getTypeHotel().getId());
			stmt.setString(3, objACreer.getNomHotel());
			stmt.setString(4, objACreer.getAdressHotel());
			stmt.setString(5, objACreer.getCpHotel());
			stmt.setString(6, objACreer.getVilleHotel());
			System.out.println(objACreer.getId()+ " "+ objACreer.getTypeHotel().getId()+ " "+objACreer.getNomHotel()+ " "+objACreer.getAdressHotel()+ " "+objACreer.getCpHotel()+ " "+objACreer.getVilleHotel() );
			int rowsInserted = stmt.executeUpdate();
			System.out.println(rowsInserted);
			if (rowsInserted > 0) {
				System.out.println("un Hotel a ete cree");
			}
		} catch (SQLException e) {
		}

		return hotel;

	}
	@Override
	public Hotel update(Hotel objAModifier) {
		Hotel hotel = null; 
		TypeHotel typeHotel = null; 

		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQUPDATE);

			stmt.setInt(1, objAModifier.getTypeHotel().getId());
			stmt.setString(2, objAModifier.getNomHotel());
			stmt.setString(3, objAModifier.getAdressHotel());
			stmt.setString(4, objAModifier.getCpHotel());
			stmt.setString(5, objAModifier.getVilleHotel());

			stmt.setInt(6, objAModifier.getId() );

			int rowsInserted = stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rowsInserted > 0) {
				System.out.println("un hotel a ete mis a jour");
				if (rs.first()) { 
					typeHotel = new DaoTypeHotel(getConnex()).findById(rs.getInt("nutype"));
					hotel = new Hotel(rs.getInt("nuhotel"), objAModifier.getTypeHotel(),
							objAModifier.getNomHotel(), objAModifier.getAdressHotel(),  objAModifier.getCpHotel(),objAModifier.getVilleHotel());

				}}


		}catch (SQLException e) {
			System.out.println("L'hotel n'a pas ete modifie");
			e.printStackTrace();
		}
		return hotel ;
	}

	//			System.out.println(objAModifier.getId()+" "+objAModifier.getTypeHotel().getId()
	//					+" "+objAModifier.getNomHotel()+" "+objAModifier.getAdressHotel()+" "+objAModifier.getCpHotel()
	//					+" "+objAModifier.getVilleHotel());
	//			ResultSet rs = stmt.getGeneratedKeys();
	//		
	//			if (rs.first()) { 
	//				typeHotel = new DaoTypeHotel(getConnex()).findById(rs.getInt("nutype"));
	//				hotel = new Hotel(rs.getInt("nuhotel"), objAModifier.getTypeHotel(),
	//						objAModifier.getNomHotel(), objAModifier.getAdressHotel(),  objAModifier.getCpHotel(),objAModifier.getVilleHotel());
	//				
	//				
	//				System.out.println(rs.getInt("nuhotel")+" "+ typeHotel.getId()+" "+
	//						rs.getString("nomhotel")+" "+  rs.getString("adressehotel")
	//						+" "+ rs.getString("cphotel")+" "+  rs.getString("villehotel"));
	//				System.out.println("un Hotel a ete mis a jour");
	//			}
	//		} catch (SQLException e) {
	//		}
	//		return hotel;
	//	}

	@Override
	public void delete(int idADeleter) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQDELETE);
			stmt.setInt(1, idADeleter);
			stmt.executeUpdate();

		}catch (SQLException e) {
		}
	}



	@Override
	public Hotel findById(int idATrouver) {
		Hotel hotel = null; 
		TypeHotel typeHotel = null; 
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQFINDBYID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setInt(1, idATrouver);
			ResultSet rs = stmt.executeQuery(); 

			if (rs.first()) {
				typeHotel = new DaoTypeHotel(getConnex()).findById(rs.getInt("nutypehotel"));
				hotel = new Hotel(rs.getInt("nuhotel"),
						typeHotel,
						rs.getString("nomhotel"),
						rs.getString("adresseHotel"), 
						rs.getString("cpHotel"),
						rs.getString("villeHotel"));
			}

		}catch (SQLException e) {;
		e.printStackTrace();
		}
		return hotel;
	}


	@Override
	public Collectionneur<Hotel> findAll() {
		Collectionneur<Hotel> Thotel = new Collectionneur<Hotel>();
		try {
			PreparedStatement stmt 	= getConnex().getCo().prepareStatement
					(REQFINDALL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs	= stmt.executeQuery();
			while (rs.next()) { 
				Thotel.addItem(new Hotel(rs.getInt("nuhotel"),
						new DaoTypeHotel(getConnex()).findById(rs.getInt("nutypehotel")),
						rs.getString("nomhotel"), rs.getString("adresseHotel"), 
						rs.getString("cpHotel"), rs.getString("villeHotel")));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}return Thotel;
	}


	@Override
	public void delete(Hotel objADeleter) {
		// TODO Auto-generated method stub

	}



}