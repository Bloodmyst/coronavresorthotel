package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Connexion.Connexion;
import model.beans.Categorie;
import model.beans.TypeHotel;
import model.model.Collectionneur;

public class DaoTypeHotel extends DAO<TypeHotel> {
	//ATTRIBUT
	private static final String REQCREATE = "INSERT INTO TYPE_HOTEL (nutype, nomtype) VALUES(?,?)";
	private static final String REQUPDATE = "UPDATE TYPE_HOTEL SET nomtype = ? WHERE nutype = ?";
	private static final String REQDELETE = "DELETE FROM TYPE_HOTEL WHERE nutype = ?";
	private static final String REQFINDBYID = "SELECT * FROM TYPE_HOTEL WHERE nutype = ?";
	private static final String REQFINDALL = "SELECT * FROM TYPE_HOTEL"; 
	//private static final String REQDELETE2 = "DELETE FROM TYPE_HOTEL WHERE nutype = ?";


	//CONSTRUCTEUR
	public DaoTypeHotel(Connexion pConnex) {
		super(pConnex);  
	}

	@Override
	public TypeHotel create(TypeHotel objACreer) {
		TypeHotel typehotel = null; 
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQCREATE);
			stmt.setInt(1, objACreer.getId());
			stmt.setString(2, objACreer.getNomType());
			
			stmt.executeUpdate();
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("un type d'hotel a ete cree");
			}
		} catch (SQLException e) {
		}
		return typehotel;
	}

	@Override
	public TypeHotel update(TypeHotel objAModifier) {

		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQUPDATE);
			stmt.setInt(2, objAModifier.getId());
			stmt.setString(1, objAModifier.getNomType());
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("un type d'hotel a ete mis a jour");
			}
		} catch (SQLException e) {
		}
		return null;
	}
	@Override
	public void delete(int idADeleter) {
		try {
			PreparedStatement stmt = getConnex().getCo().prepareStatement(REQDELETE);
			stmt.setInt(1, idADeleter);
			stmt.executeUpdate();


		}catch (SQLException e) {
		}
	}

	@Override
	public void delete(TypeHotel objADeleter) {

	}

	@Override
	public TypeHotel findById(int idATrouver) {
		TypeHotel result = null; 
		try  {
			PreparedStatement stmt 	= getConnex().getCo().prepareStatement
					(REQFINDBYID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.setInt(1, idATrouver);
			ResultSet rs	= stmt.executeQuery();
			if (rs.first()) {
				result = new TypeHotel(	rs.getInt("nutype"), 
						rs.getString("nomtype")
						);

			}
		}catch (SQLException e) {
			e.printStackTrace();}
		return result;
	}
	@Override
	public Collectionneur<TypeHotel> findAll() {
		Collectionneur<TypeHotel> types = new Collectionneur<TypeHotel>() ; 
		try  {
			PreparedStatement stmt 	= getConnex().getCo().prepareStatement
					(REQFINDALL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs	= stmt.executeQuery();
			while (rs.next()) {
				types.addItem(new TypeHotel(	rs.getInt("nutype"), 
						rs.getString("nomtype")));
			}
		}catch (SQLException e) {
			e.printStackTrace();}
		return types; 
	}


}
