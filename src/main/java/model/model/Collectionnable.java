package model.model;

public abstract class Collectionnable {
	// je choisi un "point commun" entre mes beans qui me servira de ref pour les retrouver
	//TODO je pourrai renommer Collectionnable en Referençable
	//ATTRIBUTS
	private Integer id;
	
	//CONSTRUCTEURS
	/**
	 * @param id
	 */
	public Collectionnable() {
	}
	public Collectionnable(int id) {
		this.id = id; 
	}
	//METHODES
	/**
	 * @return
	 */
	public int getId() {
		return id; 
	}
	
	
}
