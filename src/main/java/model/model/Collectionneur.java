package model.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Collectionneur <T extends Collectionnable> {
//je vais creer un hashmap comme moyen de stocker mes beans 
	//ATTRIBUTS
	private Map<Integer, T> collection = new HashMap<Integer, T>();
	
	
	
	//METHODES
	/**
	 * @param pItem
	 */
	public void addItem(T pItem) {
		collection.put(pItem.getId(), pItem); 
	}
	public T getItem(Integer itemId) {
		return collection.get(itemId);
	}
	public Map<Integer, T> getCollectionToHM(){
		return collection;
	}
	public T[] getCollectionToArray() {
		return null;
		}
	public ArrayList<T> toArray() {
		return new ArrayList<T>(collection.values());
	}
}
