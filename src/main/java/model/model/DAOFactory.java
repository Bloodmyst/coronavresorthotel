package model.model;

import Connexion.Connexion;
import model.dao.DAO;
import model.dao.DaoCategorie;
import model.dao.DaoChambre;
import model.dao.DaoTypeHotel;
import model.dao.DaoHotel;

public class DAOFactory {
	public static final int  DaoCategorie = 1; 
	public static final int DaoChambre = 2; 
	public static final int DaoTypeHotel = 3; 
	public static final int DaoHotel =4; 

	private Connexion connex= null; 

	/**
	 * @param connex
	 */
	public DAOFactory(Connexion connex) {
		this.connex=connex; 
	}
	
	
	
	public DAO getDAO(int qui) {
		switch (qui) {
		case DaoCategorie : 
			return new DaoCategorie(connex);
		case DaoChambre : 
			return new DaoChambre(connex);
		case DaoTypeHotel : 
			return new DaoTypeHotel(connex);
		case DaoHotel : 			
			return new DaoHotel(connex);
		default :
			return null;}
				

		}}

